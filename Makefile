# Software Project Course 2021/2022

# Define a project name
TARGET ?= software_project

# Utility folder name
UTILITY = utils


# $(Define PROJECT_PATH)
PROJECT_PATH = $(shell pwd)
ifeq ($(OS),Windows_NT)
	PROJECT_PATH = $(shell cd)
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		PROJECT_PATH = $(shell pwd)
	endif
	ifeq ($(UNAME_S),Darwin)
		PROJECT_PATH = $(shell pwd)
	endif
endif

# define project platform
PLATFORM ?= PLATFORM_DESKTOP

# define compiler
CC = g++
ifeq ($(OS),Windows_NT)
	CC = g++
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		CC = g++
	endif
	ifeq ($(UNAME_S),Darwin)
		CC = clang
	endif
endif

# define compiler flags
CFLAGS += -Wall -std=c++17 -D_DEFAULT_SOURCE -Wno-missing-braces

# raylib's include path
INCLUDE_PATHS := -I. -I$(PROJECT_PATH)/include

# Our include paths
INCLUDE_PATHS += -I$(PROJECT_PATH)/src
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/include
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/include/core
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/include/shape
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/include/physics
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/include
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/include/ui
# ECS
# utils
# entities
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/entities
# components
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/components
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/components/animation
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/components/core
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/components/physics
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/components/world
#systems
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/$(UTILITY)/systems
# game
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/entities
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/components
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/systems
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/helpers
INCLUDE_PATHS += -I$(PROJECT_PATH)/src/game/factories


# Library paths containing required libs
ifeq ($(OS),Windows_NT)
	LDFLAGS = -L. -L$(PROJECT_PATH)/lib/win
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		LDFLAGS = -L. -L$(PROJECT_PATH)/lib/linux
	endif
	ifeq ($(UNAME_S),Darwin)
		LDFLAGS = -L. -L$(PROJECT_PATH)/lib/mac
	endif
endif

# Libraries required on linking
ifeq ($(OS),Windows_NT)
	LDLIBS = -lraylib -lbox2d -lopengl32 -lgdi32 -lwinmm -static -lpthread
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		LDLIBS = -lraylib -lbox2d -lGL -lm -lpthread -ldl -lrt -lX11
	endif
	ifeq ($(UNAME_S),Darwin)
		LDLIBS = -lraylib -lbox2d -lc++ -framework OpenGL -framework Cocoa -framework IOKit -framework CoreAudio -framework CoreVideo
	endif
endif

# Source folders
SOURCE_FOLDERS += src src/$(UTILITY)/src
SOURCE_FOLDERS += src/$(UTILITY)/src/core
SOURCE_FOLDERS += src/$(UTILITY)/src/shape
SOURCE_FOLDERS += src/$(UTILITY)/src/physics
SOURCE_FOLDERS += src/$(UTILITY)/include/ui
SOURCE_FOLDERS += src/game/src src/game
# ECS
# utils
# entities
SOURCE_FOLDERS += src/$(UTILITY)/entities
# components
SOURCE_FOLDERS += src/$(UTILITY)/components
SOURCE_FOLDERS += src/$(UTILITY)/components/animation
SOURCE_FOLDERS += src/$(UTILITY)/components/core
SOURCE_FOLDERS += src/$(UTILITY)/components/physics
SOURCE_FOLDERS += src/$(UTILITY)/components/world
# systems
SOURCE_FOLDERS += src/$(UTILITY)/systems
# game
SOURCE_FOLDERS += src/game/entities
SOURCE_FOLDERS += src/game/components
SOURCE_FOLDERS += src/game/systems
SOURCE_FOLDERS += src/game/helpers
SOURCE_FOLDERS += src/game/factories


# Get all source files from source folders
SOURCE_FILES := $(foreach DIR, $(SOURCE_FOLDERS), $(wildcard $(DIR)/*.cpp))

# Define object files from source files
OBJECT_FILES := $(patsubst %.cpp, %.o, $(SOURCE_FILES))

# Default target
all: $(TARGET)

# Project target defined by $(TARGET)
$(TARGET): $(OBJECT_FILES)
	$(CC) -o $(TARGET) $(OBJECT_FILES) $(CFLAGS) $(INCLUDE_PATHS) $(LDFLAGS) $(LDLIBS) -D$(PLATFORM)

# Compile source files
# Pattern for compiling every "*.o" file defined on $(OBJECT_FILES)
%.o: %.cpp
	$(CC) -c $< $(CFLAGS) $(INCLUDE_PATHS) -D$(PLATFORM) -o $@

# TODO make "clean:" cross platform. linux/gnu, macOS and windows 
clean:
	rm -f $(OBJECT_FILES) $(*.o) ./$(TARGET)
