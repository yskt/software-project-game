/*
 * spgu_camera.hpp
 *
 *  Created on: Dec 22, 2021
 *      Author: christian
 */

#ifndef SRC_UTILS_INCLUDE_CORE_SPGU_CAMERA_HPP_
#define SRC_UTILS_INCLUDE_CORE_SPGU_CAMERA_HPP_


#include "raylib.h"

namespace spgu {
/**
 * @brief A class for defining camera
 * @author ysf
 *
 * This is a simple class for creating camera.
 */
class camera {
public:
	/**
	 * Constructor that creates the camera.
	 *
	 * @param window_width Width of the game window
	 * @param window_height Height of the game window
	 */
	camera(int window_width, int window_height);
	/**
	 * Getter for the camera creates the camera.
	 *
	 * @return Camera2D
	 */
	Camera2D get();

	void set_zoom(float zoom);

	void follow_centered(Vector2 target);
	void push_camera(Vector2 target);
	bool is_seeing(Vector2 position);
	void reset_offset();

private:
	/**
	 * Camera2D for the camera
	 */
	Camera2D m_camera;
	/**
	 * Vector2 for the window size
	 */
	Vector2 m_window_size;
	/**
	 * Vector2 for the bounding box scale
	 */
	Vector2 m_bounding_box_scale; // 0 == follow centered // 1 == screen edges
	/**
	 * Vector2 for the minimum bounding box
	 */
	Vector2 m_bounding_box_min;
	/**
	 * Vector2 for the maximum bounding box
	 */
	Vector2 m_bounding_box_max;
};

}

#endif /* SRC_UTILS_INCLUDE_CORE_SPGU_CAMERA_HPP_ */
