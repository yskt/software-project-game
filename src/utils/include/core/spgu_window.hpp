/*
 * spgu_window.hpp
 *
 *  Created on: Dec 22, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_INCLUDE_CORE_SPGU_WINDOW_HPP_
#define SRC_UTILS_INCLUDE_CORE_SPGU_WINDOW_HPP_


#include "raylib.h"

namespace spgu {
/**
 * @brief A class for defining window
 * @author ysf
 *
 * This is a simple class for creating game window.
 */
class window {
public:
	/**
	 * Constructor that creates the game window.
	 *
	 * @param width Width of the game window
	 * @param height Height of the game window
	 * @param title Title of the game window
	 */
	window(int width, int height, const char* title);
	/**
	 * Constructor that creates the game window.
	 *
	 * @param width Width of the game window
	 * @param height Height of the game window
	 * @param title Title of the game window
	 * @param icon_path Path to the game icon
	 */
	window(int width, int height, const char* title, const char* icon_path);
	~window();
};

}


#endif /* SRC_UTILS_INCLUDE_CORE_SPGU_WINDOW_HPP_ */
