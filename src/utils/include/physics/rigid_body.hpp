/*
 * rigid_body.hpp
 *
 *  Created on: Nov 22, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_INCLUDE_RIGID_BODY_HPP_
#define SRC_UTILS_INCLUDE_RIGID_BODY_HPP_

#include "spgu_shape.hpp"

namespace spgu {
namespace physics {
class rigid_body;
}
}

/**
 * @brief A class for defining rigid body
 * @author ysf
 *
 * This is a simple class for creating rigid body.
 */
class spgu::physics::rigid_body {
public:
	/**
	 * Constructor that creates the rigid body.
	 *
	 * @param body Shape of the body
	 */
	rigid_body(spgu::shape* body);
	spgu::shape* get_shape() {return m_body;}

//	spgu::shape::base* get_body();
	b2Body* get_body();


private:
	/**
	 * spgu::shape* for the body shape
	 */
	 spgu::shape* m_body;
};




#endif /* SRC_UTILS_INCLUDE_RIGID_BODY_HPP_ */
