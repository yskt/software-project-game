/*
 * spgu_map.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: ben
 */

#ifndef SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_HPP_
#define SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_HPP_


// std
#include <iostream>
#include <fstream>
#include <map>
#include <tuple>
#include <variant>

// external
#include "json.hpp"
#include "box2d/box2d.h"

// spgu
#include "spgu_tile_layer.hpp"
#include "spgu_object_layer.hpp"
#include "spgu_shape.hpp"


namespace spgu {
/**
 * @brief A class for importing "Tiled" map layers
 * @author ysf
 *
 * This is a simple class for importing "Tiled" map layers to the game.
 */

class map {
public:
	/**
	 * Constructor that imports the layers from a JSON file.
	 *
	 * @param path Path to the JSON file
	 * @param world Pointer to box2d world that the shapes on the object layer is created in
	 */
	map();
	map(const nlohmann::json& json, b2World* world, int col, float scale);
	map(const char* path, b2World* world, int col);
	spgu::tile_layer* get_tile_layer(std::string name);
	spgu::object_layer* get_object_layer(std::string name);

	~map();

public:
	/**
	 * map of key string and 2 variants of layer type
	 */
	int m_col;
	float m_scale;
//	float m_tile
	std::map<std::string, std::variant<spgu::tile_layer*, spgu::object_layer*>> m_map;
};
}

#endif /* SRC_UTILS_COMPONENTS_WORLD_SPGU_MAP_HPP_ */
