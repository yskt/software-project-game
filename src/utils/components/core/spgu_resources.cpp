/*
 * spgu_resources.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: ben
 */

#include "spgu_resources.hpp"

namespace spgu {


void resources::add(std::string name, std::string path, spgu::res_type type)
{
	if (type == spgu::res_type::music)
	{
		m_res.emplace(name, new spgu::music(path.c_str()));
	}
	else if (type == spgu::res_type::sound)
	{
		m_res.emplace(name, new spgu::sound(path.c_str()));
	}
	else if (type == spgu::res_type::texture)
	{
		m_res.emplace(name, new spgu::texture(path.c_str()));
	}
}

Music resources::get_music(std::string name)
{
	return std::get<spgu::music*>(m_res[name])->m_data;
}

Sound resources::get_sound(std::string name)
{
	return std::get<spgu::sound*>(m_res[name])->m_data;
}

Texture2D resources::get_texture(std::string name)
{
	return std::get<spgu::texture*>(m_res[name])->m_data;
}

}


