/*
 * spgu_story.cpp
 *
 *  Created on: Dec 22, 2021
 *      Author: crystal
 */

#include "spgu_story.hpp"

namespace spgu {

story::story(std::string story)
: m_frames_counter(0),
  m_story(story)
{}


void story::update()
{
	if (IsKeyDown(KEY_SPACE)) m_frames_counter += 8;
	else m_frames_counter++;

	if (IsKeyPressed(KEY_ENTER)) m_frames_counter = 0;
}

void story::draw(Texture2D bg)
{

	DrawTexture(bg, 0, 0, WHITE);
	DrawText(TextSubtext(m_story.c_str(), 0, m_frames_counter/10), 50, 160, 30, WHITE);
}



}  // namespace spgu

