/*
 * spgu_shape.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: yusuf
 */


#include "spgu_shape.hpp"

namespace spgu {

shape::shape(float x, float y, b2BodyType type, b2World* world)
{
	b2BodyDef bodyDef;
	bodyDef.type = type;
	bodyDef.position.Set(x, -y);
	m_body = world->CreateBody(&bodyDef);
	m_body->SetFixedRotation(true);
}

shape::~shape()
{
	m_body->GetWorld()->DestroyBody(m_body);
}

}






