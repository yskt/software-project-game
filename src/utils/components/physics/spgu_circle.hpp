/*
 * spgu_circle.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CIRCLE_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CIRCLE_HPP_


#include "box2d/box2d.h"
#include "spgu_shape.hpp"

namespace spgu {

/**
 * @brief A class for defining circle shape
 * @author ysf
 *
 * This is a simple class for creating circle shape with box2d.
 */

class circle : public spgu::shape {
public:
	/**
	 * Constructor that creates the circle shape.
	 *
	 * @param x Position in X coordinate
	 * @param y Position in Y coordinate
	 * @param radius Radius of the shape
	 * @param type Body type of the shape
	 * @param world Pointer to box2d world that the shape is created in
	 */
	circle(float x, float y, float radius, b2BodyType type, b2World* world);
	virtual ~circle();
};
}



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_CIRCLE_HPP_ */
