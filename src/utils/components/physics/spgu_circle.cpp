/*
 * spgu_circle.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: christian
 */

#include "spgu_circle.hpp"


namespace spgu {

circle::circle(float x, float y, float radius, b2BodyType type, b2World* world)
:	spgu::shape(x, y, type, world)
{
	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	// circles
	b2CircleShape circle;
	fixtureDef.shape = &circle;
	circle.m_radius = radius/2.0f;
	// circles
	circle.m_p = b2Vec2(b2Vec2_zero);
	m_body->CreateFixture(&fixtureDef);
}

circle::~circle() {}

}



