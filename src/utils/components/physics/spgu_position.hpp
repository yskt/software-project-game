/*
 * spgu_position.hpp
 *
 *  Created on: Jan 1, 2022
 *      Author: nasim
 */

#ifndef SRC_UTILS_COMPONENTS_PHYSICS_SPGU_POSITION_HPP_
#define SRC_UTILS_COMPONENTS_PHYSICS_SPGU_POSITION_HPP_

namespace spgu {
/**
 * @brief A struct for defining position in X and Y coordinates
 * @author ysf
 *
 * This is a simple struct for defining position in X and Y coordinates
 */

struct position {
	/**
	 * Constructor that creates the position.
	 *
	 * @param x Value of the X coordinate
	 * @param y Value of the Y coordinate
	 */
	position(float x, float y);
	/**
	 * float for the position in X coordinate
	 */
	float m_x;
	/**
	 * float for the position in Y coordinate
	 */
	float m_y;
};

}  // namespace spgu



#endif /* SRC_UTILS_COMPONENTS_PHYSICS_SPGU_POSITION_HPP_ */
