/*
 * tlos_lava.hpp
 *
 *  Created on: Dec 21, 2021
 *      Author: ysf
 */

#ifndef SRC_GAME_ENTITIES_TLOS_LAVA_HPP_
#define SRC_GAME_ENTITIES_TLOS_LAVA_HPP_



//
#include "entt.hpp"
#include "box2d/box2d.h"
// spgu
#include "spgu_capsule.hpp"
#include "spgu_circle.hpp"
#include "spgu_direction.hpp"
#include "tlos_entity_type.hpp"
#include "spgu_animation.hpp"
#include "tlos_entity_state.hpp"
#include "spgu_body.hpp"
#include "tlos_hitbox.hpp"
#include "tlos_hurtbox.hpp"
#include "spgu_active_animation.hpp"
#include "spgu_animation_container.hpp"


namespace tlos {
/**
 * @brief A class for defining lava
 * @author ysf
 *
 * This is a simple class for defining lava.
 */
class lava {
public:
	lava(entt::registry& registry, b2World* world);
};

}


#endif /* SRC_GAME_ENTITIES_TLOS_LAVA_HPP_ */
