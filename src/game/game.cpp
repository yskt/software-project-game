#include "game.hpp"

#define MAP_SCALE 10.0f
#define ANIM_SCALE 3.0f

tlos::game::game()
: m_world(b2World(b2Vec2_zero)),
  m_state(tlos::game_state::paused),
  m_camera(WINDOW_WIDTH, WINDOW_HEIGHT),
  m_draw(MAP_SCALE, ANIM_SCALE, 7, 8),
  m_muted(true),
  m_story(STORY)
{
//	std::string wd = std::filesystem::current_path();
#ifdef __linux__
	m_wd = new std::string(std::filesystem::canonical("/proc/self/exe"));
	m_wd->erase(m_wd->length()-16);
#endif
#ifdef __APPLE__
	*m_wd = "./";
#endif
#ifdef __MINGW32__
	*m_wd = "./";
#endif

//	m_wd = new std::string(std::filesystem::canonical("/proc/self/exe"));
//	m_wd->erase(m_wd->length()-16);
	std::ifstream map_file(*m_wd+"resources/maps/city.json");
	nlohmann::json map_json;
	map_file >> map_json;
	m_map = new spgu::map(map_json, &m_world, 27, MAP_SCALE);
	tlos::entity_importer::import(map_json, m_registry, &m_world, MAP_SCALE);

	init_resources();
	init_checkpoints();

	PlayMusicStream(m_res.get_music("st-1"));

}

tlos::game::~game()
{
	delete m_map;
}

void tlos::game::init_resources()
{
	m_res.add("tileset", *m_wd + "resources/graphics/tilesets/city.png", spgu::res_type::texture);
	m_res.add("sop_idle", *m_wd + "resources/graphics/characters/sop/idle.png", spgu::res_type::texture);
	m_res.add("sop_walk", *m_wd + "resources/graphics/characters/sop/walk.png", spgu::res_type::texture);
	m_res.add("sop_attack", *m_wd + "resources/graphics/characters/sop/attack-2.png", spgu::res_type::texture);
	m_res.add("sop_roll", *m_wd + "resources/graphics/characters/sop/roll.png", spgu::res_type::texture);
	m_res.add("bat_fly", *m_wd + "resources/graphics/characters/bat/fly.png", spgu::res_type::texture);
	m_res.add("sprinter_spin", *m_wd + "resources/graphics/characters/sprinter/fly.png", spgu::res_type::texture);
	m_res.add("wn_idle", *m_wd + "resources/graphics/characters/wn/idle.png", spgu::res_type::texture);
	m_res.add("wn_walk", *m_wd + "resources/graphics/characters/wn/walk.png", spgu::res_type::texture);
	m_res.add("wn_attack", *m_wd + "resources/graphics/characters/wn/attack.png", spgu::res_type::texture);
	m_res.add("st-1", *m_wd + "resources/sound/st-1.mp3", spgu::res_type::music);
	m_res.add("computer", *m_wd + "resources/graphics/ui/computer.png", spgu::res_type::texture);
	m_res.add("play", *m_wd + "resources/graphics/ui/play.png", spgu::res_type::texture);
	m_res.add("pause", *m_wd + "resources/graphics/ui/pause.png", spgu::res_type::texture);
	m_res.add("story", *m_wd + "resources/graphics/ui/story.png", spgu::res_type::texture);
	m_res.add("help", *m_wd + "resources/graphics/ui/help.png", spgu::res_type::texture);
	m_res.add("exit", *m_wd + "resources/graphics/ui/exit.png", spgu::res_type::texture);
	m_res.add("back", *m_wd + "resources/graphics/ui/back.png", spgu::res_type::texture);
	m_res.add("menu_bg", *m_wd + "resources/graphics/ui/menu_bg.png", spgu::res_type::texture);
	m_res.add("controls", *m_wd + "resources/graphics/ui/controls.png", spgu::res_type::texture);
	m_res.add("controls_2", *m_wd + "resources/graphics/ui/controls_2.png", spgu::res_type::texture);
	m_res.add("health_bar", *m_wd + "resources/graphics/ui/health_bar.png", spgu::res_type::texture);
	m_res.add("health_bar1", *m_wd + "resources/graphics/ui/health_bar1.png", spgu::res_type::texture);
	m_res.add("player_life", *m_wd + "resources/graphics/ui/player_life.png", spgu::res_type::texture);
	m_res.add("heart", *m_wd + "resources/graphics/ui/heart.png", spgu::res_type::texture);
	m_res.add("coin", *m_wd + "resources/graphics/ui/coin.png", spgu::res_type::texture);
	m_res.add("diamond", *m_wd + "resources/graphics/ui/diamond.png", spgu::res_type::texture);
	m_res.add("controller", *m_wd + "resources/graphics/ui/controller.png", spgu::res_type::texture);
	m_res.add("stop", *m_wd + "resources/graphics/ui/stop_button_in_game_3.png", spgu::res_type::texture);
	m_res.add("map", *m_wd + "resources/graphics/ui/map-20x22.png", spgu::res_type::texture);
	m_res.add("help_map", *m_wd + "resources/graphics/ui/help_map.png", spgu::res_type::texture);
	m_res.add("coin3", *m_wd + "resources/graphics/ui/coin3.png", spgu::res_type::texture);
	m_res.add("win_page", *m_wd + "resources/graphics/ui/win_page_bg.png", spgu::res_type::texture);
	m_res.add("game_over_page", *m_wd + "resources/graphics/ui/game_over_page_bg.png", spgu::res_type::texture);
	m_res.add("mute_2", *m_wd + "resources/graphics/ui/mute_2.png", spgu::res_type::texture);
	m_res.add("pb", *m_wd + "resources/graphics/ui/pb.png", spgu::res_type::texture);
	m_res.add("story_bg", *m_wd + "resources/graphics/ui/story_bg_1.png", spgu::res_type::texture);
}

void tlos::game::reset()
{
	entt::basic_view view = m_registry.view<spgu::shape*>();
	for (entt::entity e : view)
	{
		spgu::shape*& collider = view.get<spgu::shape*>(e);
		delete collider;
	}

#ifdef __linux__
	m_wd = new std::string(std::filesystem::canonical("/proc/self/exe"));
	m_wd->erase(m_wd->length()-16);
#endif
#ifdef __APPLE__
	*m_wd = "./";
#endif
#ifdef __MINGW32__
	*m_wd = "./";
#endif


	m_registry.clear();
	std::ifstream map_file(*m_wd + "resources/maps/city.json");
	nlohmann::json map_json;
	map_file >> map_json;
	tlos::entity_importer::import(map_json, m_registry, &m_world, MAP_SCALE);
	init_checkpoints();
}

void tlos::game::update()
{
	m_movement.move(m_registry);
	m_movement.follow_player(m_registry);
	m_player_state.update(m_registry);
	m_boss_state.update(m_registry);

	m_melee.attack(m_registry);
	m_melee.take_damage(m_registry);
	sop_controller::decide_animation(m_registry);
	m_sop_pos = tlos::get_sop::position(m_registry);
	m_camera.push_camera({m_sop_pos.x, -m_sop_pos.y});
	m_world.Step(1.0f/FPS, 8, 3);

	update_health();

	UpdateMusicStream(m_res.get_music("st-1"));
	if (m_muted) PauseMusicStream(m_res.get_music("st-1"));
	else ResumeMusicStream(m_res.get_music("st-1"));
}

void tlos::game::draw()
{
	BeginDrawing();
	BeginMode2D(m_camera.get());
		ClearBackground(DARKGRAY);

		if (m_state == tlos::game_state::running)
		{
			m_draw.map(m_res, *m_map, "ground1", m_sop_pos);
			m_draw.map(m_res, *m_map, "ground2", m_sop_pos);
			m_draw.map(m_res, *m_map, "under1", m_sop_pos);
			m_draw.map(m_res, *m_map, "under2", m_sop_pos);
			m_draw.animations(m_res, m_registry);
			m_draw.map(m_res, *m_map, "above", m_sop_pos);

			bonus();
			sop_win();

			//m_draw.debug(&m_world);
			//tlos::draw::debug(m_registry);
		}

	EndMode2D();
	if (m_state == tlos::game_state::paused) paused();
	else if (m_state == tlos::game_state::running) running();
	else if (m_state == tlos::game_state::story) story();
	else if (m_state == tlos::game_state::help) help();
	else if (m_state == tlos::game_state::game_over) game_over();
	else if (m_state == tlos::game_state::win) win();
	EndDrawing();
}

void tlos::game::run()
{
	SetTargetFPS(FPS);
	while (!WindowShouldClose() && m_state != tlos::game_state::close)
	{
		if (m_state == tlos::game_state::running)
		{
			update();
		}
		if (m_state == tlos::game_state::story)
		{
			m_story.update();
		}
		draw();
	}
}

void tlos::game::running()
{

	entt::basic_view health = m_registry.view<tlos::player_type, tlos::character_stats>();
	for (entt::entity e : health)
	{
		tlos::character_stats& stats = health.get<tlos::character_stats>(e);

		if (stats.m_lives <= 0)
		{
			m_state = tlos::game_state::game_over;
		}
	}


	DrawRectangle(0, 0, WINDOW_WIDTH, 80, {0,0,0,80});
	m_muted = m_gui.checkbox(m_res.get_texture("mute_2"), {WINDOW_WIDTH - 164.0f, 10.0f, 60.0f, 60.0f}, m_muted);

	if (m_gui.image_button({WINDOW_WIDTH - 85.0f, 10.0f, 60.0f, 60.0f},  m_res.get_texture("stop")))
	{
		m_state = tlos::game_state::paused;
	}

	DrawRectangle(0, WINDOW_HEIGHT - 40, 100, 40, {0, 0, 0, 200});
	DrawFPS(10, WINDOW_HEIGHT - 30);

	entt::basic_view view_health1 = m_registry.view<tlos::player_type, tlos::character_stats>();
	for (entt::entity e : view_health1)
	{
		tlos::character_stats& stats = view_health1.get<tlos::character_stats>(e);

		m_gui.progress_bar(m_res.get_texture("pb"), {15.0f, 19.0f, 54*5, 8*5}, 5, stats.m_health, stats.m_max_health);
		m_gui.n_times(m_res.get_texture("heart"), 310.0f, 5.0f, 4.0f, stats.m_lives, 1);

		DrawTexturePro(m_res.get_texture("diamond"), {0.0f, 0.0f, 16.0f, 16.0f}, {658.0f, 5.0f, 64.0f, 64.0f}, {0.0f, 0.0f}, 0.0f, WHITE);
		DrawText(TextFormat("%i", stats.m_score), 730, 12, 54, BLACK);

		//DrawTexturePro(m_res.get_texture("coin3"), {0.0f, 0.0f, 16.0f, 16.0f}, {794.0f, 5.0f, 64.0f, 64.0f}, {0.0f, 0.0f}, 0.0f, WHITE);
		//DrawText(TextFormat("%i", stats.m_score), 866, 12, 54, BLACK);
	}

	help_map();

}

void tlos::game::paused()
{
	DrawTexture(m_res.get_texture("menu_bg"), 0, 0, WHITE);
	if (m_gui.image_button({WINDOW_WIDTH - 450.0f, WINDOW_HEIGHT/2.0f-300.0f, 300.0f, 150.0f}, m_res.get_texture("play")))
	{
		m_state = tlos::game_state::running;
	}
	if (m_gui.image_button({WINDOW_WIDTH - 450.0f, WINDOW_HEIGHT/2.0f-150.0f, 300.0f, 150.0f}, m_res.get_texture("help")))
	{
		m_state = tlos::game_state::help;
	}
	if (m_gui.image_button({WINDOW_WIDTH - 450.0f, WINDOW_HEIGHT/2, 300.0f, 150.0f}, m_res.get_texture("story")))
	{
		m_state = tlos::game_state::story;

	}
	if (m_gui.image_button({WINDOW_WIDTH - 450.0f, WINDOW_HEIGHT/2.0f+150.0f, 300.0f, 150.0f}, m_res.get_texture("exit")))
	{
		m_state = tlos::game_state::close;
	}

}

void tlos::game::story()
{
	m_story.draw(m_res.get_texture("story_bg"));

	if (m_gui.image_button({WINDOW_WIDTH/2.0f - 350.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("back")))
	{
		m_state = tlos::game_state::paused;
		m_story.m_frames_counter = 0;
	}

	if (m_gui.image_button({WINDOW_WIDTH/2.0f + 50.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("play")))
	{
		m_state = tlos::game_state::running;
	}
}

void tlos::game::help()
{
	DrawTexture(m_res.get_texture("controls_2"), 0, 0, WHITE);
	DrawText("Game Controls", 120, 70, 70, WHITE);
	DrawText("Character Controls", 200, 400, 30, WHITE);
	DrawText("Roll (speed)", 125, 570, 30, WHITE);
	DrawText("Pick", 430, 570, 30, WHITE);
	DrawText("Attack", 580, 570, 30, WHITE);
	DrawText("How to play?", 800, 250, 30, WHITE);
	DrawText(HOW_TO_PLAY.c_str(), 700, 300, 20, WHITE);
	if (m_gui.image_button({WINDOW_WIDTH/2.0f - 350.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("back")))
	{
		m_state = tlos::game_state::paused;
	}

	if (m_gui.image_button({WINDOW_WIDTH/2.0f + 50.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("play")))
	{
		m_state = tlos::game_state::running;
	}
}

void tlos::game::win()
{
	DrawTexture(m_res.get_texture("win_page"), 0, 0, WHITE);
	entt::basic_view sop = m_registry.view<tlos::player_type, tlos::character_stats>();
		tlos::character_stats* stats;
		for (entt::entity s : sop)
		{
			tlos::character_stats& st = sop.get<tlos::character_stats>(s);
			stats = &st;
		}
	DrawText("YOU WIN", 350, 250, 100, WHITE);
	DrawText("Score: ", 400, 600, 50, WHITE);
	DrawText(TextFormat("%i", stats->m_score), 600, 600, 50, WHITE);
	if (m_gui.image_button({WINDOW_WIDTH/2.0f - 350.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("back")))
	{
		m_state = tlos::game_state::paused;
		reset();
	}

	if (m_gui.image_button({WINDOW_WIDTH/2.0f + 50.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("play")))
	{
		m_state = tlos::game_state::running;
		reset();
	}
}

void tlos::game::game_over()
{
	entt::basic_view sop = m_registry.view<tlos::player_type, tlos::character_stats>();
		tlos::character_stats* stats;
		for (entt::entity s : sop)
		{
			tlos::character_stats& st = sop.get<tlos::character_stats>(s);
			stats = &st;
		}

	DrawTexture(m_res.get_texture("game_over_page"), 0, 0, WHITE);
	DrawText("GAME OVER", 600, 400, 75, WHITE);
	DrawText("Score: ", 700, 500, 50, WHITE);
	DrawText(TextFormat("%i", stats->m_score), 900, 500, 50, WHITE);


	if (m_gui.image_button({WINDOW_WIDTH/2.0f - 350.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("back")))
	{
		reset();
		m_state = tlos::game_state::paused;
	}

	if (m_gui.image_button({WINDOW_WIDTH/2.0f + 50.0f, WINDOW_HEIGHT - 175.0f, 300.0f, 150.0f},m_res.get_texture("play")))
	{
		reset();
		m_state = tlos::game_state::running;
	}
}

void tlos::game::init_checkpoints()
{

	entt::basic_view view = m_registry.view<spgu::box, spgu::point_order>();
	for (entt::entity e : view)
	{
		spgu::box& box = view.get<spgu::box>(e);
		spgu::point_order& po = view.get<spgu::point_order>(e);
		m_check_points[po.m_data] = {box.m_box.x/100.0f+WINDOW_WIDTH-170.0f,  box.m_box.y/100.0f+WINDOW_HEIGHT-170.0f};
	}
}


void tlos::game::help_map()
{
	entt::basic_view view = m_registry.view<spgu::box, spgu::point_order>();

		// small game map
		DrawTexturePro
		(
				m_res.get_texture("help_map"),
				{0.0f,0.0f,160.0f,160.0f},
				{WINDOW_WIDTH-170.0f , WINDOW_HEIGHT-170.0f, 160.0f,160.0f},
				{0.0f},
				0.0f,
				{255, 255, 255, 200}
		);
		DrawRectangleLinesEx
		(
				{WINDOW_WIDTH-175.0f , WINDOW_HEIGHT-175.0f, 170.0f,170.0f},
				5.0f,
				BLACK
		);

		for (entt::entity e : view)
		{
			spgu::box& box = view.get<spgu::box>(e);
			DrawRectanglePro({box.m_box.x/100.0f+WINDOW_WIDTH-170.0f-5.0f, box.m_box.y/100.0f+WINDOW_HEIGHT-170.0f-5.0f, 10.0f,10.0f}, {0.0f,0.0f}, 0.0f, BLACK);
			//DrawText(TextFormat("%i", po.m_data), box.m_box.x/100+WINDOW_WIDTH-190, box.m_box.y/100+WINDOW_HEIGHT-200, 20, BLACK);
		}

		for (int i = 1; i < 19; i++)
		{
			DrawLineEx(m_check_points[i], m_check_points[i+1], 5.0f, BLACK);
		}

		DrawRectanglePro({m_sop_pos.x/100.0f+WINDOW_WIDTH-170.0f-5, -m_sop_pos.y/100.0f+WINDOW_HEIGHT-170.0f-5.0f, 10.0f,10.0f}, {0.0f,0.0f}, 0.0f, RED);
}

void tlos::game::bonus()
{
	entt::basic_view sop = m_registry.view<
			tlos::player_type, tlos::hurtbox, tlos::character_stats>();
	tlos::hurtbox* hb;
	tlos::character_stats* stats;
	for (entt::entity s : sop)
	{
		tlos::hurtbox& h = sop.get<tlos::hurtbox>(s);
		tlos::character_stats& st = sop.get<tlos::character_stats>(s);
		hb = &h;
		stats = &st;
	}

	entt::basic_view view = m_registry.view<
			spgu::position,
			spgu::active_animation,
			spgu::draw_quad,
			spgu::timer,
			tlos::bonus_item_type>();

	for (entt::entity e : view)
	{
		spgu::position& pos = view.get<spgu::position>(e);
		spgu::active_animation& anim = view.get<spgu::active_animation>(e);
		spgu::draw_quad& dq = view.get<spgu::draw_quad>(e);
		spgu::timer& timer = view.get<spgu::timer>(e);
		tlos::bonus_item_type& item_type = view.get<tlos::bonus_item_type>(e);
		timer.m_current += GetFrameTime();
		Rectangle rec =
		{
				pos.m_x + dq.m_offset_x - dq.m_width/2,
				pos.m_y + dq.m_offset_y - dq.m_height/2,
				dq.m_width,
				dq.m_height
		};
		DrawTexturePro
		(
				m_res.get_texture(anim.m_name),
				{0.0f, 0.0f, 16.0f, 16.0f},
				rec,
				{0.0f, 0.0f},
				0.0f,
				WHITE)
		;

		if (item_type.m_type == tlos::item_type::diamond)
		{
			if (CheckCollisionRecs(rec, hb->m_hurtbox) || timer.m_current >= timer.m_limit)
			{
				m_registry.destroy(e);

			}
			if (CheckCollisionRecs(rec, hb->m_hurtbox))
			{
				++stats->m_score;
			}
		}

	}
}

void tlos::game::update_health()
{
	entt::basic_view health = m_registry.view<tlos::player_type, tlos::character_stats>();
	for (entt::entity e : health)
	{
		tlos::character_stats& stats = health.get<tlos::character_stats>(e);
		if (stats.m_lives <= 0)
		{
			m_state = tlos::game_state::game_over;
		}
	}
}

void tlos::game::sop_win()
{
	entt::basic_view sop = m_registry.view<tlos::player_type, tlos::hurtbox>();
	tlos::hurtbox* hurtbox;
	for (entt::entity s : sop)
	{
		tlos::hurtbox& hb = sop.get<tlos::hurtbox>(s);
		hurtbox = &hb;
	}

	entt::basic_view controller = m_registry.view<
			spgu::position,
			spgu::draw_quad,
			tlos::bonus_item_type>();

	for (entt::entity e : controller)
	{
		tlos::bonus_item_type& item_type = controller.get<tlos::bonus_item_type>(e);
		if (item_type.m_type == tlos::item_type::controller)
		{
			spgu::position& pos = controller.get<spgu::position>(e);
			spgu::draw_quad& dq = controller.get<spgu::draw_quad>(e);
			Rectangle rec =
			{
					pos.m_x + dq.m_offset_x - dq.m_width/2,
					pos.m_y + dq.m_offset_y - dq.m_height/2,
					dq.m_width,
					dq.m_height
			};

			if (CheckCollisionRecs(hurtbox->m_hurtbox, rec) && IsKeyDown(KEY_C))
			{
					m_registry.destroy(e);
					m_state = tlos::game_state::win;
			}
		}

	}
}


