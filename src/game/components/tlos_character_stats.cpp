/*
 * tlos_character_stats.cpp
 *
 *  Created on: Dec 22, 2021
 *      Author: angelov
 */

#include "tlos_character_stats.hpp"

namespace tlos {

character_stats::character_stats
(
		int lives,
		int max_health,
		int attack
)	:	m_level(0),
		m_experience(0),
		m_experience_next(1000),
		m_lives(lives),
		m_max_health(max_health),
		m_health(1000),
		m_attack(attack),
		m_score(0)
{}

character_stats::character_stats
(
		int level,
		int experience,
		int experience_next,
		int lives,
		int max_health,
		int health,
		int attack
)	:	m_level(level),
		m_experience(experience),
		m_experience_next(experience_next),
		m_lives(lives),
		m_max_health(max_health),
		m_health(health),
		m_attack(attack),
		m_score(0)
{}


}




