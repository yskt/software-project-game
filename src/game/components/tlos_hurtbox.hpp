/*
 * tlos_hurtbox.hpp
 *
 *  Created on: Dec 20, 2021
 *      Author: angelov
 */

#ifndef SRC_GAME_COMPONENTS_TLOS_HURTBOX_HPP_
#define SRC_GAME_COMPONENTS_TLOS_HURTBOX_HPP_

#include <iostream>
#include "raylib.h"

namespace tlos {
/**
 * @brief A struct for defining hurtbox
 * @author angelov
 *
 * This is a simple struct for defining hurtbox.
 */
struct hurtbox {
	hurtbox(float width, float height, float offset_x, float offset_y);
	~hurtbox();
	//
	Rectangle m_hurtbox;
	Vector2 m_offset;
};

}



#endif /* SRC_GAME_COMPONENTS_TLOS_HURTBOX_HPP_ */
