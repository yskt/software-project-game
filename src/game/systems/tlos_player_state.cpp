/*
 * tlos_player_state.cpp
 *
 *  Created on: Dec 20, 2021
 *      Author: nasim, christian
 */

#include "tlos_player_state.hpp"

namespace tlos {

player_state::player_state() : m_time(0.0f), m_attack_cooldown(0.3), m_roll_cooldown(0.7)
{}

void player_state::update(entt::registry& registry)
{
	entt::basic_view view = registry.view<tlos::entity_state, tlos::entity_type>();

	for (entt::entity e : view)
	{
		tlos::entity_type& type = view.get<tlos::entity_type>(e);
		if (type.m_type == tlos::type::player)
		{
			tlos::entity_state& state = view.get<tlos::entity_state>(e);
			if (state.m_state == tlos::state::idle) idle(state);
			else if (state.m_state == tlos::state::walk) walk(state);
			else if (state.m_state == tlos::state::attack) attack(state);
			else if (state.m_state == tlos::state::roll) roll(state);
		}
	}
}

void player_state::idle(tlos::entity_state& state)
{
	if (IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT))
		state.m_state = tlos::state::walk;
	else if (IsKeyPressed(KEY_X)) state.m_state = tlos::state::attack;
}

void player_state::walk(tlos::entity_state& state)
{
	if (!IsKeyDown(KEY_UP) && !IsKeyDown(KEY_DOWN) && !IsKeyDown(KEY_RIGHT) && !IsKeyDown(KEY_LEFT))
	{
		state.m_state = tlos::state::idle;
	}
	else if (IsKeyPressed(KEY_X))
	{
		state.m_state = tlos::state::attack;
	}
	else if ((IsKeyPressed(KEY_SPACE)))
	{
		state.m_state = tlos::state::roll;
	}
}

void player_state::attack(tlos::entity_state& state)
{
	m_time += GetFrameTime();
	if ((m_time >= m_attack_cooldown) && (!IsKeyDown(KEY_UP) && !IsKeyDown(KEY_DOWN) && !IsKeyDown(KEY_RIGHT) && !IsKeyDown(KEY_LEFT)))
	{
		state.m_state = tlos::state::idle;
		m_time = 0.0f;
	}
	else if ((m_time >= m_attack_cooldown) && (IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT)))
	{
		state.m_state = tlos::state::walk;
		m_time = 0.0f;
	}
}

void player_state::roll(tlos::entity_state& state)
{
	m_time += GetFrameTime();
	if ((m_time >= m_roll_cooldown) && (IsKeyDown(KEY_UP) || IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT)))
	{
		state.m_state = tlos::state::walk;
		m_time = 0.0f;
	}
	else if (m_time >= m_roll_cooldown)
	{
		state.m_state = tlos::state::idle;
		m_time = 0.0f;
	}
}

//
}

