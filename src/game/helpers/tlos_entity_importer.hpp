/*
 * tlos_enitity_importer.hpp
 *
 *  Created on: Dec 24, 2021
 *      Author: ysf
 */

#ifndef SRC_GAME_TLOS_ENTITY_IMPORTER_HPP_
#define SRC_GAME_TLOS_ENTITY_IMPORTER_HPP_

#include <iostream>
#include <string>
#include "entt.hpp"
#include "json.hpp"
#include "raylib.h"
#include "box2d/box2d.h"

#include "tlos_sop.hpp"
#include "tlos_bat.hpp"
#include "tlos_sprinter.hpp"
#include "tlos_checkpoint.hpp"
#include "tlos_diamond.hpp"
#include "tlos_boss.hpp"

namespace tlos {
/**
 * @brief A class which imports entities from json file
 * @author yusuf
 *
 * This is a class which imports entities from json file.
 */
struct entity_importer {

	static void import(const nlohmann::json& json, entt::registry& registry, b2World* world, float scale);

};

}



#endif /* SRC_GAME_TLOS_ENITITY_IMPORTER_HPP_ */
